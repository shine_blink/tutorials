# Mqtt 接入天翼云（CTwing）



> 本教程适合 FlexLua SmartSensor 和 FlexLua Locator 系列硬件。



#### 一、实现目标

通过 Mqtt 接入天翼云，并将传感器数据、GPS 定位数据及 UTC 时间信息以 Json 形式发给云服务器。Json 消息形式举例如下（以温湿度传感器为例）：

```lua
{"Uid":"Test01","Temperature":25.2, "Humidity":76.7}
```

或

```lua
{"Uid":"Test01","Longtitude":119.2, "Latitude":26.1, "UTC":1679488305,"Temperature":25.2, "Humidity":76.7}
```



#### 二、设备接入云指导

> 天翼云MQTT官方教程：https://www.ctwing.cn/sbjr/39#see
>
> 天翼云入口：https://www.ctwing.cn/，进入后在屏幕右上角点击进入“控制台”。

#### （1）在AEP控制台创建产品（MQTT透传模式接入）

​			创建产品的参数可参考下图：

<img src="img/CtwingProductDetails.png" alt="CtwingProductDetails" style="zoom:50%;" />

#### （2）在上一步创建好的产品中的《设备管理》中添加一台设备

​			创建每台设备时注意保持“设备编号”的唯一性，例如下图中的设备编号为 “DTU01”

<img src="img/CtwingDeviceDetails.png" alt="CtwingDeviceDetails" style="zoom:50%;" />





#### （3）设备参数配置

​			在天翼云后台添加好设备之后，用 USB Type-C 数据线来配置设备的参数。

​			下面仅列出和 Mqtt 接入天翼云相关的参数：

```lua
--Part3: MQTT参数（SysMode=1时才有用）
MqttServerAddr = "mqtt.ctwing.cn" --天翼云MQTT服务器ip地址或域名
MqttServerPort = 1883 --MQTT服务器端口号
MqttClientID = "15589964DTU01" --产品ID"15589964" + 设备编号"DTU01"
MqttUserName = "ShineBlink" --建议填写为用户自己的天翼物联网平台（AIoT)用户名
MqttPassword = "lMmugH2yURmY2uJqkNby-zQHGJ67ngjMAYR6kkGQmko" --采用的一型一密，特征串
MqttSubTopic = "device_control"--订阅用，用于接收服务器下发的数据
MqttPubTopic = "$device_send" --发布用，用于向服务器发送数据
```

注意：[MqttClientID] 和 [MqttPassword] 一定要是你的设备 ID 和特征串，不要照抄上面的。



#### 三、上电测试

给设备上电后，等待设备绿灯亮了之后表示设备已接入天翼云，接下来每隔一段时间，在天翼云后台的数据查看里可以看到设备上发的 Json 数据，形式如下：

```lua
{"Uid":"Test01","Longtitude":119.2, "Latitude":26.1, "UTC":1679488305,"Temperature":25.2, "Humidity":76.7}
```

实际截图如下：

![pic1](img/pic1.png)