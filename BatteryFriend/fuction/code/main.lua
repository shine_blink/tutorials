----------------------配置信息开始----------------------
--Led灯配置
--如果GreenLedFlash=1时，每2秒绿色LED闪0.1秒，指示系统正在供电运行
--如果想节约更多电，就把GreenLedFlash=0
GreenLedFlash = 1 
--RS485 Modbus参数配置
MbAddr = 254 --modbus本机地址, 1~255
MbBaudRate = "BAUDRATE_9600" --485通信速率，必须和被供电设备的波特率一致
----------------------配置信息结束----------------------

--定义10毫秒定时器的回调函数，函数名字必须是LIB_10msTimerCallback
function LIB_10msTimerCallback()
	LIB_GpioToggle("D11") --喂硬件看门狗
	if GreenLedFlash == 1 then
		--绿色LED灯处理程序
		LED_G_OnTimeMsCnt = LED_G_OnTimeMsCnt + 10
		--如果GreenLedFlash=1时，每2秒绿色LED闪0.1秒
		if LED_G_OnTimeMsCnt <= 100 then
			LIB_GpioWrite("D1",0) --亮
		elseif LED_G_OnTimeMsCnt <= 2000 then
			LIB_GpioWrite("D1",1) --灭
		else
			LED_G_OnTimeMsCnt = 0
			LIB_GpioWrite("D1",1) --灭
		end
	end
	--获取电池电压（一秒出一次结果）
	res,vol = GetBatteryVoltage()
	if res == 1 then
		BatteryVoltage = vol
	end
end

--RGB三色灯控制函数
function RGB(R,G,B)
	LIB_GpioWrite("D0",R)
	LIB_GpioWrite("D1",G)
	LIB_GpioWrite("D3",B)
end

--采集电池电压,注意：该函数需要耗时1.1秒以上
function GetBatteryVoltage()
	local bat_vol
	local res
	bat_vol = 0.0
	res = 0
	--获取A0通道的20个点电池电压并求平均值（1秒范围）
    An_full_flag, An_tab = LIB_ADCheckBufFull("A0")
    if An_full_flag == 1 then
		res = 1
        SUM = 0
        for i = 1, #An_tab do --此处#An_tab的值是20，表示An_tab表内的元素个数
            SUM = SUM + An_tab[i]
        end
        AVER = SUM / #An_tab
        bat_vol = AVER*3.6/4096.0 --求20个点的平均值以及对应的实际电压值
		bat_vol = bat_vol*6.1 --(510K+100K)/100K
    end
	return res, bat_vol
end

--ModBus 从机通信函数及功能实现
function ModbusProcess()
    local sdata = {}
	local RegStartAddr
	local RegNum
	local RegValue
	local SingleCoilAddr
	local SingleCoilVal
	local CoilOp
    
    --查询是否收到Modbus主机发来的消息
    flag, data = LIB_Uart1Recv()
    if flag == 1 then
        --判断消息是不是发给本机，是本机的才理会
        if data[1] == MbAddr then --MbAddr是Modbus本机地址
			print("Rx:"..LIB_HexTabToHexStr(data))
            --判断Modbus功能码
            if data[2] == 0x03 then --0x03 读多个保持寄存器
                --定义地址为1000的寄存器存放电池电压，且读取的寄存器个数必须是1个
				RegStartAddr = data[3] << 8 | data[4]
				RegNum = data[5] << 8 | data[6]
				print(string.format("StartAddr:%d,Num:%d",RegStartAddr,RegNum))
                if RegStartAddr == 1000 and RegNum == 1 then
                    sdata[1] = data[1] --本机地址
                    sdata[2] = data[2] --功能码
                    sdata[3] = 2 --数据域字节数: 1个寄存器一共2字节
					Voltage = BatteryVoltage*10 --电压单位:0.1v
                    sdata[4] = math.floor(Voltage) >> 8
                    sdata[5] = math.floor(Voltage) & 0x00ff
                    CRC = LIB_CrcCalculate("CRC16_MODBUS", sdata)
                    sdata[6] = CRC & 0x00ff --低位在前
                    sdata[7] = CRC >> 8 --高位在后
					print("Tx:"..LIB_HexTabToHexStr(sdata))
                    --回复数据
                    LIB_Uart1BlockSend(sdata)
                else
					print("Invalid modbus addr or num")
                    --回复异常消息（非法数据地址）
                    sdata[1] = data[1] --本机地址
                    sdata[2] = data[2]+0x80 --异常的时候功能码加0x80
                    sdata[3] = 0x02 --异常码0x02表示设备不支持此数据地址
                    CRC = LIB_CrcCalculate("CRC16_MODBUS", sdata)
                    sdata[4] = CRC & 0x00ff --低位在前
                    sdata[5] = CRC >> 8 --高位在后
                    LIB_Uart1BlockSend(sdata)
                end
			elseif data[2] == 0x05 then -- 0x05 写单个线圈
                --定义地址为1-4的线圈地址作为通道Mos1-4开关（最大支持2A）通道的开关信号
				SingleCoilAddr = data[3] << 8 | data[4] --地址：1~4
				SingleCoilVal = data[5] << 8 | data[6] --打开：0XFF00 关闭：0x0000
				print(string.format("CoilAddr:%d,Value:%d",RegStartAddr,SingleCoilVal))
				print("Tx:"..LIB_HexTabToHexStr(data))
                if SingleCoilAddr == 1 then--Channal 1
					if SingleCoilVal == 0xff00 then --open
						LIB_GpioWrite("D5",1)
					elseif SingleCoilVal == 0x0000 then --close
						LIB_GpioWrite("D5",0)
					else
						print("Invalid coil1 val")
					end
                    --回复数据(将收到的数据原封不动返回)
                    LIB_Uart1BlockSend(data)
				elseif SingleCoilAddr == 2 then--Channal 2
					if SingleCoilVal == 0xff00 then --open
						LIB_GpioWrite("D6",1)
					elseif SingleCoilVal == 0x0000 then --close
						LIB_GpioWrite("D6",0)
					else
					
					end
                    --回复数据(将收到的数据原封不动返回)
                    LIB_Uart1BlockSend(data)
				elseif SingleCoilAddr == 3 then--Channal 3
					if SingleCoilVal == 0xff00 then --open
						LIB_GpioWrite("D7",1)
					elseif SingleCoilVal == 0x0000 then --close
						LIB_GpioWrite("D7",0)
					else
					
					end
                    --回复数据(将收到的数据原封不动返回)
                    LIB_Uart1BlockSend(data)
				elseif SingleCoilAddr == 4 then--Channal 4
					if SingleCoilVal == 0xff00 then --open
						LIB_GpioWrite("D9",1)
					elseif SingleCoilVal == 0x0000 then --close
						LIB_GpioWrite("D9",0)
					else
					
					end
                    --回复数据(将收到的数据原封不动返回)
                    LIB_Uart1BlockSend(data)
                else
					print("Invalid modbus addr or num")
                    --回复异常消息（非法数据地址）
                    sdata[1] = data[1] --本机地址
                    sdata[2] = data[2]+0x80 --异常的时候功能码加0x80
                    sdata[3] = 0x02 --异常码0x02表示设备不支持此数据地址
                    CRC = LIB_CrcCalculate("CRC16_MODBUS", sdata)
                    sdata[4] = CRC & 0x00ff --低位在前
                    sdata[5] = CRC >> 8 --高位在后
                    LIB_Uart1BlockSend(sdata)
                end
			elseif data[2] == 0x06 then -- 0x06 写单个保持寄存器
                --定义地址为1001的寄存器作为休眠时间设置值，如果该值大于零则触发休眠
				RegStartAddr = data[3] << 8 | data[4]
				RegValue = data[5] << 8 | data[6]
				print(string.format("StartAddr:%d,Value:%d",RegStartAddr,RegValue))
                if RegStartAddr == 1001 and RegValue > 0 then
					print("Tx:"..LIB_HexTabToHexStr(data))
                    --回复数据(将收到的数据原封不动返回)
                    LIB_Uart1BlockSend(data)
					--根据RegValue的秒数，来决定系统休眠的时间
					LIB_AM1805Sleep(RegValue, "NONE")--此时系统(core开发板)将掉电，后面的代码都不再有效了
                else
					print("Invalid modbus addr or num")
                    --回复异常消息（非法数据地址）
                    sdata[1] = data[1] --本机地址
                    sdata[2] = data[2]+0x80 --异常的时候功能码加0x80
                    sdata[3] = 0x02 --异常码0x02表示设备不支持此数据地址
                    CRC = LIB_CrcCalculate("CRC16_MODBUS", sdata)
                    sdata[4] = CRC & 0x00ff --低位在前
                    sdata[5] = CRC >> 8 --高位在后
                    LIB_Uart1BlockSend(sdata)
                end
            else
                --回复异常消息（非法功能码）
                sdata[1] = data[1] --本机地址
                sdata[2] = data[2]+0x80 --异常的时候功能码加0x80
                sdata[3] = 0x01 --异常码0x01表示设备不支持此功能码
                CRC = LIB_CrcCalculate("CRC16_MODBUS", sdata)
                sdata[4] = CRC & 0x00ff --低位在前
                sdata[5] = CRC >> 8 --高位在后
                LIB_Uart1BlockSend(sdata)
            end
        end
    end
end

--LIB_SystemLogEnable() --打开详细日志(LOG.TXT)
--配置D0,D1,D3为普通GPIO输出,控制LED_R,LED_G,LED_B
LIB_GpioOutputConfig("D0","STANDARD")
LIB_GpioOutputConfig("D1","STANDARD")
LIB_GpioOutputConfig("D3","STANDARD")
RGB(1,1,1) --一上电Led三色灯默认都不亮
--配置D5,D6,D7,D9为普通GPIO输出,控制4路Mos开关模块
LIB_GpioOutputConfig("D5","STANDARD")
LIB_GpioWrite("D5",0)--Ch1 默认不开
LIB_GpioOutputConfig("D6","STANDARD")
LIB_GpioWrite("D6",0)--Ch2 默认不开
LIB_GpioOutputConfig("D7","STANDARD")
LIB_GpioWrite("D7",0)--Ch3 默认不开
LIB_GpioOutputConfig("D9","STANDARD")
LIB_GpioWrite("D9",0)--Ch4 默认不开
--配置D4为普通GPIO输出,外界Mos开关模块控制AD采集电池电压电路是否供电
LIB_GpioOutputConfig("D4","STANDARD")
LIB_GpioWrite("D4",1)--AD采集电池电压电路开始供电
--配置D11为普通输出,控制看门狗 
LIB_GpioOutputConfig("D11","STANDARD")
--配置Uart1串口波特率，并且D8口在uart发送时会自动变成低电平切换收发
LIB_Uart1Rs485Config(MbBaudRate, "D8")
--配置A0-A3这四个通道同时开始工作(A0通道用来采集电池电压)
--当每个通道采集满20个点时缓存满，每个点的采集时间间隔为50ms
LIB_ADConfig(20,50000)
--配置AM1805低功耗管理模块连接SDA0和SCL0引脚
LIB_AM1805Config("IIC0")
--使能系统10毫秒定时器开始工作
LIB_10msTimerConfig("ENABLE")
LED_G_OnTimeMsCnt = 0
BatteryVoltage = 0.0

--开始大循环
while(GC(1) == true)
do
	ModbusProcess()
end
