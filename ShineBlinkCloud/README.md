# 接入 ShineBlink 免开发云和微信小程序



> 本教程适合 FlexLua SmartSensor 和 FlexLua Locator 系列硬件。



#### 一、实现目标

接入 ShineBlink 免开发云和小程序，并将传感器数据发给云服务器，并可在微信小程序中的仪表盘上看到传感器的数据（以温湿度传感器为例）。



#### 二、微信二维码生成

二维码一般会在产品背面提供，如果没有也可以通过复制以下文本，并将文本放在二维码生成工具中生成即可（例如 [https://cli.im](https://cli.im/) ）

```
https://shinemng.shineblink.com/static/miniprograms/?code=SBK_01;761a6617e803f78402
```

注意：您需要将上面的 761a6617e803f78402 替换成您自己的序列号，该序列号可在虚拟U盘的 log.txt 文件最开始的内容中的 **UID** 看到，另外需注意的是 log.txt 中的 **UID** 是大写且带空格，一定要将它们转换成小写并去掉空格。例如我们在下面的 log.txt 文件中找到 UID：

![pic2](img/pic2.png)

那么我们用来生成的二维码字符串应该是：

```
https://shinemng.shineblink.com/static/miniprograms/?code=SBK_01;fa2e9e4bdc03f78402
```



#### 三、上电测试

给设备上电后，等待设备绿灯亮了之后表示设备已接入云，这时可以用微信扫码二维码，接下来每隔一段时间，在微信小程序里可以看到设备上发的传感器数据，微信截图如下：

<img src="img/pic1.jpg" alt="pic1" style="zoom: 33%;" />

