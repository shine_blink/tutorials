# TCP 接入安信可透传测试服务器



> 本教程适合 FlexLua SmartSensor 和 FlexLua Locator 系列硬件。



#### 一、实现目标

通过 TCP 接入安信可提供的免费测试 TCP 服务器，并将传感器数据、GPS 定位数据及 UTC 时间信息以 Json 形式发给安信可透传云服务器。Json 消息形式举例如下（以温湿度传感器为例）：

```lua
{"Uid":"Test01","Temperature":25.2, "Humidity":76.7}
```

或

```lua
{"Uid":"Test01","Longtitude":119.2, "Latitude":26.1, "UTC":1679488305,"Temperature":25.2, "Humidity":76.7}
```



#### 二、配置参数介绍

下面仅列出和 TCP 接入相关的参数：

```lua
--Part2: Tcp参数（SysMode=0时才有用）
TcpServerIp = "122.114.122.174" --需要连接的安信可TCP服务器IP地址
TcpServerPort = 33210 --需要连接的安信可TCP服务器端口号
```



#### 三、云接入指导

下面使用安信可TCP透传云实际测试，该透传云特别适合没有云服务器的开发者临时拿来测试使用，但该服务器的 IP 地址和端口号只在三分钟内有效，如果三分钟内没接入则将失效（需要重新刷新浏览器页面），所以测试的时候需要动作快一些。

首先浏览器打开：http://tt.ai-thinker.com/

然后根据网页上显示的 IP 地址和端口号，快速更改设备的配置信息并重新上电，接下来就可以看到设备将传感器信息上传到服务器并在网页端显示出上传的数据。具体需要修改的参数如下：

```lua
--Part2: Tcp参数（SysMode=0时才有用）
TcpServerIp = "122.114.122.174" --需要连接的服务器IP地址
TcpServerPort = 33210 --需要连接的服务器端口号
```

最后实际测试后我们可以在网页端看到下图中的内容，由于本测试实例中我们用的 SB-FL02 带 GPS 和温湿度传感器的设备向云传输数据，所以传输的数据形式为：

```lua
{"Uid":"Test01","Longtitude":119.2, "Latitude":26.1, "UTC":1679488305,"Temperature":25.2, "Humidity":76.7}
```

截图如下：

![pic1](img/pic1.png)